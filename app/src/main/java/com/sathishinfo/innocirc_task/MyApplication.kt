package com.sathishinfo.innocirc_task

import android.content.Context
import androidx.multidex.MultiDexApplication
import com.sathishinfo.innocirc_task.data.IMoviesService
import com.sathishinfo.innocirc_task.data.MoviesFactory

class MyApplication : MultiDexApplication() {

    private var moviesService: IMoviesService? = null

    companion object {

        private operator fun get(context: Context): MyApplication? {
            return context.applicationContext as MyApplication
        }
        open fun create(context: Context): MyApplication? {
            return get(context)
        }

    }

    fun getMoviesService(): IMoviesService? {
        if (moviesService == null) {
            moviesService = MoviesFactory.create()
        }
        return moviesService
    }


}