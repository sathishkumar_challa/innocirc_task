package com.sathishinfo.innocirc_task.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sathishinfo.innocirc_task.R
import com.sathishinfo.innocirc_task.databinding.MoviesActivityBinding
import com.sathishinfo.innocirc_task.viewmodel.MainViewModel
import java.util.*

class MainActivity : AppCompatActivity(), Observer {

    private var moviesActivityBinding: MoviesActivityBinding? = null
    private var mainViewMode: MainViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initDataBinding()
        setupListMoviesView(moviesActivityBinding?.listMovies)
        setupObserver(mainViewMode)


    }

    private fun setupObserver(mainViewMode: MainViewModel?) {
       var observer= mainViewMode as Observable
        observer.addObserver(this)

    }

    private fun setupListMoviesView(listMovies: RecyclerView?) {
        var adapter: MoviesAdapter = MoviesAdapter()
        listMovies?.layoutManager = LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL, false)
        listMovies?.adapter = adapter

    }

    private fun initDataBinding() {
        moviesActivityBinding = DataBindingUtil.setContentView(this, R.layout.movies_activity)
        mainViewMode = MainViewModel(this)
        moviesActivityBinding?.mainViewModel = mainViewMode
    }


    override fun update(observable: Observable?, arg: Any?) {

        if (observable is MainViewModel) {
            val adapter: MoviesAdapter =
                moviesActivityBinding?.listMovies?.getAdapter() as MoviesAdapter
            val peopleViewModel: MainViewModel = observable
            adapter.setMoviesList(peopleViewModel.getMoviesList())
        }
    }

}
