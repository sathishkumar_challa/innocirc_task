package com.sathishinfo.innocirc_task.view;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.sathishinfo.innocirc_task.R;
import com.sathishinfo.innocirc_task.databinding.ItemMovieBinding;
import com.sathishinfo.innocirc_task.model.MoviesModel;
import com.sathishinfo.innocirc_task.viewmodel.ItemMovieViewModel;

import java.util.Collections;
import java.util.List;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MoviesAdapterViewHolder> {

    private List<MoviesModel> moviesList;

    MoviesAdapter() {
        this.moviesList = Collections.emptyList();
    }

    @NonNull
    @Override
    public MoviesAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemMovieBinding itemPeopleBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_movie,
                        parent, false);
        return new MoviesAdapterViewHolder(itemPeopleBinding);
    }

    @Override
    public void onBindViewHolder(MoviesAdapterViewHolder holder, int position) {
        holder.bindPeople(moviesList.get(position));
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    void setMoviesList(List<MoviesModel> moviesList) {
        this.moviesList = moviesList;
        notifyDataSetChanged();
    }

    static class MoviesAdapterViewHolder extends RecyclerView.ViewHolder {
        ItemMovieBinding mItemMovieBinding;

        MoviesAdapterViewHolder(ItemMovieBinding mItemMovieBinding) {
            super(mItemMovieBinding.itemMovieRl);
            this.mItemMovieBinding = mItemMovieBinding;
        }

        void bindPeople(MoviesModel movie) {
            if (mItemMovieBinding.getMoviesModel() == null) {
                ItemMovieViewModel model= new ItemMovieViewModel(movie, itemView.getContext());
                mItemMovieBinding.setImageUrl(model.getMovieImage());
                mItemMovieBinding.setMoviesModel(model);

            } else {
                mItemMovieBinding.setImageUrl(movie.getImage());
                mItemMovieBinding.getMoviesModel().setMovie(movie);

            }
        }
    }
}
