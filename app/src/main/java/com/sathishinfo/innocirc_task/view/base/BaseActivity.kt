package com.sathishinfo.innocirc_task.view.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.sathishinfo.innocirc_task.viewmodel.base.BaseViewModel

abstract class BaseActivity<VM : BaseViewModel?> : AppCompatActivity() {
    protected var viewModel: VM? = null
    protected abstract fun createViewModel(): VM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = createViewModel()
    }
}