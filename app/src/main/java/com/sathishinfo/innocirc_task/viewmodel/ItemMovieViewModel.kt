package com.sathishinfo.innocirc_task.viewmodel

import android.content.Context
import android.view.View
import android.widget.ImageView
import androidx.databinding.BaseObservable
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.sathishinfo.innocirc_task.model.MoviesModel


class ItemMovieViewModel(private var movie: MoviesModel, private val context: Context) :
    BaseObservable() {
    val fullName: String?
        get() = movie.title

    val movieImage: String?
        get() = movie.image

    val type: String?
        get() = movie.type

    val language: String?
        get() = movie.language

    val rating: String?
        get() = movie.rating

    fun onItemClick(view: View?) {}

    fun setMovie(movie: MoviesModel) {
        this.movie = movie
        notifyChange()
    }

    companion object {
       /* @BindingAdapter("imageUrl")
        fun setImageUrl(imageView: ImageView, url: String?) {
            Glide.with(imageView.context).load(url).into(imageView)
        }*/
        /*@BindingAdapter("android:src")
        fun setImageViewResource(imageView: ImageView, url: String?) {
           // imageView.setImageResource(resource)
           Glide.with(imageView.context).load(url).into(imageView)
        }*/

        @BindingAdapter("profileImage")
        @JvmStatic
        fun loadImage(view: ImageView, imageUrl: String?) {
            Glide.with(view.context)
                .load(imageUrl)
                .into(view)
        }

    }





}