package com.sathishinfo.innocirc_task.viewmodel

import android.content.Context
import android.view.View
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.sathishinfo.innocirc_task.MyApplication
import com.sathishinfo.innocirc_task.R
import com.sathishinfo.innocirc_task.data.MoviesFactory
import com.sathishinfo.innocirc_task.model.MoviesModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.*

class MainViewModel(context: Context) : Observable() {
    var moviesRecycler: ObservableInt
    var emptyLabel: ObservableInt
    var headerLabel: ObservableInt
    private val moviesList: MutableList<MoviesModel>
    private var context: Context?
    private var compositeDisposable: CompositeDisposable? = CompositeDisposable()
    var messageLabel: ObservableField<String>
    fun initializeViews() {
        moviesRecycler.set(View.GONE)
    }

    private fun fetchPeopleList() { // MyApplication application = MyApplication.Companion.create(context);
        val service = MyApplication.create(context!!)!!.getMoviesService()
        val disposable = service!!.fetchMovies(MoviesFactory.MOVIES_URL)
            ?.subscribeOn(Schedulers.newThread())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({ moviesModelList ->
                changeMoviesDataSet(moviesModelList as List<MoviesModel>)
                messageLabel.set("")
                moviesRecycler.set(View.VISIBLE)
                emptyLabel.set(View.GONE)
                headerLabel.set(View.VISIBLE)
            }) { throwable ->
                messageLabel.set("Please check network connection.\nError: " + throwable.message)
                moviesRecycler.set(View.GONE)
                emptyLabel.set(View.VISIBLE)
                headerLabel.set(View.GONE)
                throwable.printStackTrace()
            }
        disposable?.let { compositeDisposable!!.add(it) }
    }

    private fun changeMoviesDataSet(peoples: List<MoviesModel>) {
        moviesList.addAll(peoples!!)
        setChanged()
        notifyObservers()
    }

    fun getMoviesList(): List<MoviesModel> {
        return moviesList
    }

    private fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable!!.isDisposed) {
            compositeDisposable!!.dispose()
        }
    }

    fun reset() {
        unSubscribeFromObservable()
        compositeDisposable = null
        context = null
    }

    init {
        this.context = context
        moviesList = ArrayList()
        moviesRecycler = ObservableInt(View.GONE)
        emptyLabel = ObservableInt(View.GONE)
        headerLabel= ObservableInt(View.GONE)
        messageLabel =
            ObservableField(context.getString(R.string.empty_loading_movies))
        initializeViews()
        fetchPeopleList()
    }
}