package com.sathishinfo.innocirc_task.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class MoviesModel {
    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("image")
    @Expose
    var image: String? = null
    @SerializedName("is_new")
    @Expose
    var isNew: Int? = null
    @SerializedName("rating")
    @Expose
    var rating: String? = null
    @SerializedName("like_percent")
    @Expose
    var likePercent: Int? = null
    @SerializedName("vote_count")
    @Expose
    var voteCount: Int? = null
    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("language")
    @Expose
    var language: String? = null
    @SerializedName("type")
    @Expose
    var type: String? = null

}