package com.sathishinfo.innocirc_task.data

import com.sathishinfo.innocirc_task.model.MoviesModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Url

interface IMoviesService {
    @GET
    fun fetchMovies(@Url url: String?): Observable<List<MoviesModel?>?>?
}