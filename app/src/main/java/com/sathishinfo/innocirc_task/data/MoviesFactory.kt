package com.sathishinfo.innocirc_task.data

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object MoviesFactory {
    const val BASE_URL = "https://api.simplifiedcoding.in/"
    const val MOVIES_URL = BASE_URL + "course-apis/recyclerview/movies"
    fun create(): IMoviesService {
        val retrofit = Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
        return retrofit.create(IMoviesService::class.java)
    }
}